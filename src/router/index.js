import Vue from 'vue'
import Router from 'vue-router'
import Login from '@/components/Login'
import CommunityIndex from '@/components/CommunityIndex'
import technicalIndex from '@/components/technicalIndex'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/Community',
      name: 'Community',
      component: CommunityIndex
    },
    {
      path: '/',
      name: 'Login',
      component: Login
    },
    {
      path: '/technicalIndex',
      name: 'technicalIndex',
      component: technicalIndex
    },
  ]
})
